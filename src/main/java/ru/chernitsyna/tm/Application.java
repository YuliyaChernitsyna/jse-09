package ru.chernitsyna.tm;

import ru.chernitsyna.tm.controller.ProjectController;
import ru.chernitsyna.tm.controller.SystemController;
import ru.chernitsyna.tm.controller.TaskController;
import ru.chernitsyna.tm.repository.ProjectRepository;
import ru.chernitsyna.tm.repository.TaskRepository;
import ru.chernitsyna.tm.service.ProjectService;
import ru.chernitsyna.tm.service.TaskService;

import java.util.Scanner;

import static ru.chernitsyna.tm.constant.TerminalConst.*;

public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskController taskController = new TaskController(taskService);

    private final SystemController systemController = new SystemController();

    {
        projectService.create("DEMO PROJECT 1");
        projectService.create("DEMO PROJECT 2");
        taskService.create("TEST TASK 1");
        taskService.create("TEST TASK 2");
    }

    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application application = new Application();
        application.run(args);
        application.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            application.run(command);
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case HELP: return systemController.displayHelp();
            case VERSION: return systemController.displayVersion();
            case ABOUT: return systemController.displayAbout();
            case EXIT: return systemController.displayExit();

            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID: return projectController.updateProjectById();

            case TASK_CREATE: return taskController.createTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_LIST: return taskController.listTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID: return taskController.updateTaskById();

            default: return systemController.displayError();
        }
    }


}
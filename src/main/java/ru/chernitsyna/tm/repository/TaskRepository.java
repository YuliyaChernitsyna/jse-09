package ru.chernitsyna.tm.repository;

import ru.chernitsyna.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        tasks.add(task);
        return task;
    }

    public int getRepositorySize() {
        return tasks.size();
    }

    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        tasks.add(task);
        return task;
    }

    public Task updateByIndex(final int index, final String name, final String description) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setDescription(description);
        task.setName(name);
        return task;
    }

    public Task updateById(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setDescription(description);
        task.setName(name);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public Task findByIndex(final int index) {
        return tasks.get(index);
    }

    public Task findByName(final String name) {
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    public Task findById(final Long id) {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }


    public List<Task> findAll() {
        return tasks;
    }

}